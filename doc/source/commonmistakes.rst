common mistakes
===============

Here is a list of hints to find out why your reconstruction don't fit with your expectations.

Transmission
------------

* the freeart transmission algorithm is considering that sinograms are the result of :math:`log(\frac{I_t}{I_0})`. But if this is not it for some reason and the sinogram is in fact the result of :math:`\frac{I_t}{I_0}` then you sould uncheck the 'Compute -log' in the normalization tab.

Fluorescence
------------

* detector position is taking into account so please make sur the one given is correct
* solid angle is taking into account. You can force it to be force to one for all point sample (see reconstruction parameters tab)
* absorption and self attenuation are taking into account. The value of those are saved for each pixel in :math:`g.cm^{-2}` (`see <http://www.edna-site.org/pub/doc/freeart/latest/io_data.html>`_). And the calculation of the absorptions are also based on the voxel size !!!
  So a wrong value of it can bring either no effect on the reconstruction or a too important and bring reconstruction like no phantom or 'fiew voxel reconstruction'.
* damping factor: this is an important parameter for an art reconstruction. By default a plausible value is setted. But incoherent value can bring to strange behavior to. If too low then you might bring a 'blur' effect and the solution will never converge. If to hight you will have some really incoherent phantom.


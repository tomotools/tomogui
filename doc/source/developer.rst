developer information
=====================

Configuration file
''''''''''''''''''

The configuration file is based on the one provided by freeart.
So both are compatible and you can run freeart reconstruction using the same file by calling the freeart interpreter.
Here for example from the command line:

.. code-block:: bash

    freeart interpreter [configurationFile]


`Here is the link to the freeart configuration documentation <http://www.edna-site.org/pub/doc/freeart/latest/modules/configuration/index.html>`_

.. toctree::
   :hidden:

   configuration.rst

:doc:`configuration`
    A few information about the configuration/project file of tomogui

:doc:`api`
    A first draw for the API (used by PyMCA)

overview
========

tomogui is a offering a graphical User Interface to run tomographic reconstructions.
For now tomogui is using the following algorithm:

* `Fast Back Projection <http://www.silx.org/doc/silx/latest/modules/image/backprojection.html>`_ using the `silx <http://www.silx.org/doc/silx/latest/>`_. It is a GPU based implementation. So in order to access this algorithm, you should be correctly installed and configure opencl and pyopencl.
* `ART Transmission <http://www.edna-site.org/pub/doc/freeart/latest/transmission.html>`_ using `freeart <http://www.edna-site.org/pub/doc/freeart/latest/>`_.
* `ART Fluorescence <http://www.edna-site.org/pub/doc/freeart/latest/fluorescence.html>`_ using `freeart <http://www.edna-site.org/pub/doc/freeart/latest/>`_.

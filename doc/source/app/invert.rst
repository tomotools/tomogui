tomogui invert
--------------

This small application take a single frame .edf file and perform the following operation:

   :math:`data.max() - data` and store the result in the second given file.

.. code-block:: bash

    tomogui invert fileDataToInvert.edf fileDataToSave.edf


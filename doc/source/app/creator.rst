.. _tomogui-creator:

tomogui creator
---------------

This simple interface can generate absorption matrix.

.. snapshotqt:: img/tomoguicreator.png

    from tomogui.gui.creator.AbsMatCreator import AbsMatCreator
    creator = AbsMatCreator()
    creator.show()

Defining sample composition
"""""""""""""""""""""""""""

In order to add materials and define canvas size you have to load a background image (using the 'load background image' button)

Then you can apply multiple material using the addMaterial button and the draw tools.

Once the sample composition fit your need you can save this composition by using the Save button.
Later you will be able to load back this composition using the load button.


Generating absorption matrix
""""""""""""""""""""""""""""

From a sample composition you can generate the corresponding absorption matrix for a given energy using the "Generate absorption matrix" group.

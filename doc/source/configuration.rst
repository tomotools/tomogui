project configuration
=====================

The tomogui project as freeart is able to deal with .h5 and .cfg/.ini files
But there is some small behavior differences:
- only when the project is saved in .h5 the option 'save all data in the .h5 configuration file' is availaible
- if the project is define as a .cfg/.ini then file for materials may be asked

.. note:: freeart should be able to deal without those differences but the gui is not up to deal with all cases.
API
===

Submodules
----------

tomogui\.gui\.MainWidget module
-------------------------------

.. automodule:: tomogui.gui.MainWidget
    :members:
    :undoc-members:
    :show-inheritance:


tomogui\.gui\.ProjectWidget module
----------------------------------

.. automodule:: tomogui.gui.ProjectWidget
    :members:
    :undoc-members:
    :show-inheritance:

Installation
============

tomogui is a python library offering GUI for some tomography reconstruction algorithm.

.. note:: you will need to have a gitlab account to get the rights in order to clone the git project.

To install if download the source code :

.. code-block:: bash
    :emphasize-lines: 3,5

    git clone git@gitlab.esrf.fr:tomoTools/tomogui.git
    pip install .

    # then make sure the unit tests are succeeding
    python run_tests.py


To build the documentation :

.. code-block:: bash
    :emphasize-lines: 3,5

    python setup.py build_doc

Reconstruction algorithm
""""""""""""""""""""""""

tomogui is actually able to run:

- FBP algorithm using silx
- ART algorithm (transmission and fluorescence algorithms) using freeart.

.. warning:: To run those algorithm the corresponding 'base' library need to be installed.


Simple example to generate a freeart compatible matrix using numpy
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

.. code-block:: bash
    :emphasize-lines: 3,5

    n = numpy.zeros((56, 56), dtype=numpy.float64)
    # ... here do motification of your matrix
    reconstrutils.saveMatrix
    reconstrutils.saveMatrixToTxt

    reconstrutils.saveMatrix(n, "matrix.edf")

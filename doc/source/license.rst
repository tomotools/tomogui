License
=======

The source code of *tomogui* is mostly licensed under the `MIT <https://opensource.org/licenses/MIT>`.
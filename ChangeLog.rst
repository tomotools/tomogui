Change Log
==========

0.3.1:
------

- fix six import issue
- update screenshot directive to fit the one of silx.

0.3.0:
------

- use silx.io.url to save get the file information, to store configurations and use silx.io.util.loadData to merge some code.
- use the latest silx.gui.dialog.ImageFileDialog

0.2.0: 2018/03/16
-----------------

- deal with h5 files for saving/loading project, matrices, material definition
- add sinogramselector
- fix bug with fisx: create a unique material name
- add continuous integration
- add examples for fluorescence and transmission from .h5 files
- add tutorial for fluorescence reconstruction
- fix fisx issue when name wasn't unique was taking existing material definition if already existing
- deal with several sinograms for Tx and FBP
- move scripts to use the silx system. tomoGUIproject-> tomogui project and tomoGUIrecons -> tomogui recons
- add applications creator, invert, materials and norm
- add the configuration module as third party
- add ImageFileDialog as third party

0.1.0 2017/07/21:
-----------------

- add scripts tomoGUIproject and tomoGUIrecons
- add precision management from the interpreter
- deal with several fluorescence sinogram
- can store project in .cfg files
- draw materials composition from a mask
- compatibility python 2 - python 3
